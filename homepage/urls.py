from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.beranda, name='beranda'),
    path('tentang/', views.tentang, name='tentang'),
    path('kontak/', views.kontak, name='kontak'),
    path('pengalaman/', views.pengalaman, name='pengalaman'),
    path('galeri/', views.galeri, name='galeri'),
    path('kegiatan/', views.kegiatan, name = 'kegiatan'),
    path('tambah/', views.tambah_kegiatan, name = 'tambah'),
    path('pilih/<kegiatan_id>', views.pilih, name='pilih'),
    path('hapus/', views.hapusKegiatan, name='delete'),
]
