from django import forms

class Formnya(forms.Form):
    namaKegiatan = forms.CharField(max_length = 50,
                                   widget = forms.TextInput(attrs = {
                                       'class' : 'form-control',
                                       'placeholder' : 'ex : Tidur'}))
    tanggalKegiatan = forms.DateField(widget = forms.DateInput(attrs = {
                                        'class' : 'form=control',
                                        'placeholder' : '',
                                        'type':'date'}))
    tempatKegiatan = forms.CharField(max_length = 50,
                                   widget = forms.TextInput(attrs = {
                                       'class' : 'form-control',
                                       'placeholder' : 'ex : Kamar'}))
    jamKegiatan = forms.TimeField(widget = forms.TimeInput(attrs = {
                                        'class' : 'form-control',
                                        'type' : 'time'}))
    kategoriKegiatan = forms.CharField(max_length = 50,
                                   widget = forms.TextInput(attrs = {
                                       'class' : 'form-control',
                                       'placeholder' : 'ex : Istirahat'}))
    
    
