# Generated by Django 2.2.5 on 2019-10-08 14:47

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Kegiatan',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('namaKegiatan', models.CharField(max_length=50)),
                ('tanggalKegiatan', models.DateField()),
                ('tempatKegiatan', models.CharField(max_length=50)),
                ('jamKegiatan', models.TimeField()),
                ('kategoriKegiatan', models.CharField(max_length=50)),
            ],
        ),
    ]
